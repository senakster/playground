import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  title = 'contact';
  email = 'senakster@gmail.com';

  constructor() { }

  ngOnInit(): void {
  }

}
