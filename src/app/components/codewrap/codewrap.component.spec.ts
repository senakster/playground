import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CodewrapComponent } from './codewrap.component';

describe('CodewrapComponent', () => {
  let component: CodewrapComponent;
  let fixture: ComponentFixture<CodewrapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodewrapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodewrapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
