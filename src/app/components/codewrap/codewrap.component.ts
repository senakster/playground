import { Component, Input, OnInit } from '@angular/core';
import { Codewrapping } from '@src/app/interfaces/codewrap';

@Component({
  selector: 'app-codewrap',
  templateUrl: './codewrap.component.html',
  styleUrls: ['./codewrap.component.css']
})
export class CodewrapComponent implements OnInit {
  @Input() msg: Codewrapping;
  pinned = false;
  codeDisplay = '';

  togglePinned(): void {
    this.pinned = !this.pinned;
  }

  constructor() { }

  ngOnInit(): void {
    this.selectCodeType('description');
    window.onscroll = () => {
      this.pinned = window.pageYOffset === 0 ? false : this.pinned;
    };
  }

  jumpCode(): void {
    this.pinned = true;
    const scrollEl = document.getElementById('codeContainer');
      window.scroll({
        top: scrollEl.offsetTop,
        behavior: 'smooth'
      });
  }

  jumpTop(): void {
    this.pinned = false;
      window.scroll({
        top: 0,
        behavior: 'smooth'
      });
  }

  selectCodeType(type: string): void {
    this.codeDisplay = type;
  }
}
