import { Component, ComponentFactoryResolver, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectsDirective, ProjectsList } from '@src/app/projects/projects.directive';
import { NavigationService } from '@src/app/services/navigation.service';

@Component({
  selector: 'app-snippets',
  templateUrl: './snippets.component.html',
  styleUrls: ['./snippets.component.css']
})
export class SnippetsComponent implements OnInit {
  @ViewChild(ProjectsDirective, {static: true}) projectHost: ProjectsDirective;



  title = 'snippets';
  projects = [
    {url: 'imagemerger' ,
    name: 'Watermarker'},
    {url: 'videobackground' ,
    name: 'Video Background'},
    {url: 'stopwatch',
    name: 'RxJs Stopwatch'},
    {url: 'restclient',
    name: 'REST client'}
  ];

  currentProject: string = null;


  constructor(
    private projectsDirective: ProjectsDirective,
    private componentFactoryResolver: ComponentFactoryResolver,
    private route: ActivatedRoute,
    private navService: NavigationService,
  ) { }

  ngOnInit(): void {
    // console.log(this.projects.map((v) => v.url));
      if (this.projects.map((v) => v.url).includes(this.route.snapshot.paramMap.get('project'))) {
      this.view(this.route.snapshot.paramMap.get('project'));
      } else {

      }
    }

  nav(dest: string): void {
    this.navService.nav(dest);
  }
  view(project: string): void {
    this.currentProject = project;
    this.loadComponent(project);
      const scrollEl = document.getElementById('projectView');
      window.scroll({
        top: window.pageYOffset + scrollEl.getBoundingClientRect().top,
        behavior: 'smooth'
      });
  }
  load(project: string): void {
    // some redundancy in loading process. Could probably be made smarter
    this.currentProject = project;
    this.view(project);
    this.nav(`snippets/${project}`); // seo/deep linking purposes only
  }


  /*
  * Load component from projects.directive
  * @Param project {{string}}
  */
  loadComponent(project: string): void {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(ProjectsList[project]);
    const viewContainerRef = this.projectHost.viewContainerRef;
    viewContainerRef.clear();
    const componentRef = viewContainerRef.createComponent(componentFactory);
    // componentRef.instance.data = ProjectsList[project].data;
  }
  menu(): void {
  const viewContainerRef = this.projectHost.viewContainerRef;
  viewContainerRef.clear();
  this.currentProject = null;
  }
}
