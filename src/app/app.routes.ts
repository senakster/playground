import { Routes } from '@angular/router';

import { HomeComponent } from '@src/app/home/home.component';
import { ContactComponent } from './components/contact/contact.component';
import { ErrComponent } from './components/err/err.component';
import { SnippetsComponent } from './components/snippets/snippets.component';
import { AboutComponent } from './components/about/about.component';

export const routes: Routes = [
  {
      path: '',
      redirectTo: '/home',
      pathMatch: 'full',
  },
  {
      path: 'home',
      component: HomeComponent,
      data: { animation: 'HomeComponent' }
  },
  {
      path: 'snippets',
      component: SnippetsComponent,
      data: { animation: 'SnipetsComponent' }
  },
  {
      path: 'contact',
      component: ContactComponent,
      data: { animation: 'ContactComponent' }
  },
  {
    path: 'about',
    component: AboutComponent,
    data: { animation: 'aboutComponent' }
  },
  {
      path: 'snippets/:project',
      component: SnippetsComponent,
      data: { animation: 'SnippetsComponent' }
  },
  {
      path: '**',
      component: ErrComponent,
      data: { animation: 'ErrComponent' }
  },
];
