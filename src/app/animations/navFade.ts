import {
    animation, trigger, animateChild, group,
    transition, animate, style, query
  } from '@angular/animations';

  export const NavFade = animation([
    style({
      height: '{{ height }}',
      opacity: '{{ opacity }}',
      backgroundColor: '{{ backgroundColor }}'
    }),
    animate('{{ time }}')
  ]);

export const fader =
  trigger('navAnimation', [
    transition('* <=> *', [
      // Set a default  style for enter and leave
      query(':enter, :leave', [

        style({
          position: 'absolute',
          left: '0',
          width: '100%',
          opacity: 0
        }),
      ], {optional: true}),
      // Animate the new page in
      query(':enter', [
        animate('600ms', style({ opacity: 1 })),
      ], {optional: true})
    ]),
]);
