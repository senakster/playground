import { Component, EventEmitter, Output } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { fader } from '@src/app/animations/navFade';

@Component({
  selector: 'app-root',
  animations: [
    fader
  ],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  theme = 'light';
  title = 'Playground';

  toggleTheme() {
    this.theme = this.theme === 'dark' ? 'light' : 'dark';
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }
}
