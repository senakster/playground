import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from '@src/app/app-routing.module';

// material module
import { MatIconModule } from '@angular/material/icon';

// main components
import { AppComponent } from '@src/app/app.component';
import { HomeComponent } from '@src/app/home/home.component';
import { NavigationComponent } from '@src/app/navigation/navigation.component';
import { ContactComponent } from '@src/app/components/contact/contact.component';
import { ErrComponent } from './components/err/err.component';
import { AboutComponent } from './components/about/about.component';

import { SnippetsComponent } from '@src/app/components/snippets/snippets.component';
import { CodewrapComponent } from './components/codewrap/codewrap.component';


// projects components
import { VideobackgroundComponent } from './projects/videobackground/videobackground.component';
import { ImagemergerComponent } from './projects/imagemerger/imagemerger.component';
import { RestclientComponent } from './projects/restclient/restclient.component';
import { StopwatchComponent } from './projects/stopwatch/stopwatch.component';

// snippets project loading tool
import { ProjectsDirective } from './projects/projects.directive';

// Tools Modules
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HighlightModule, HIGHLIGHT_OPTIONS } from 'ngx-highlightjs';
import { FormsModule } from '@angular/forms';
import { AngularCropperjsModule } from 'angular-cropperjs';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavigationComponent,
    ContactComponent,
    SnippetsComponent,
    VideobackgroundComponent,
    ImagemergerComponent,
    ErrComponent,
    ProjectsDirective,
    CodewrapComponent,
    StopwatchComponent,
    AboutComponent,
    RestclientComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatIconModule,
    AngularCropperjsModule,
    HttpClientModule,
    HighlightModule,
    FormsModule,
    DragDropModule,
  ],
  providers: [
    {
      provide: HIGHLIGHT_OPTIONS,
      useValue: {
        coreLibraryLoader: () => import('highlight.js/lib/core'),
        languages: {
          typescript: () => import('highlight.js/lib/languages/typescript'),
          css: () => import('highlight.js/lib/languages/css'),
          xml: () => import('highlight.js/lib/languages/xml')}
      }
    },
    HttpClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
