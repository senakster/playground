import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImagemergerComponent } from './imagemerger.component';

describe('ImagemergerComponent', () => {
  let component: ImagemergerComponent;
  let fixture: ComponentFixture<ImagemergerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImagemergerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagemergerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
