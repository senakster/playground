import { Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { Observable, Subject, fromEvent, interval, NEVER } from 'rxjs';
import { map, scan, startWith, switchMap, tap, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { State, StateEvent } from '@src/app/interfaces/state';
import { Codewrapping } from '@src/app/interfaces/codewrap';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-stopwatch',
  templateUrl: './stopwatch.component.html',
  styleUrls: ['./stopwatch.component.css']
})
export class StopwatchComponent implements OnInit {
  code: Codewrapping = {};
  title = 'Event Sourcing - Stopwatch';
  count = 0;
  UIInput: Observable<any>[];
  UIOutput: Subject<any> = new Subject();
  initState: State = {
    count: false,
    speed: 1000,
    countvalue: 0,
    countup: true,
    increment: 1
  };
  state: State;
  set = {speed: this.initState.speed, countvalue: this.initState.countvalue, increment: this.initState.increment };
  counter;
  @ViewChildren('input') input: ElementRef[];
  @ViewChildren('button') button: ElementRef[];

  constructor(
    private http: HttpClient
  ) {
  }

  ngOnInit(): void {
    this.state = this.initState;
    this.UIOutput
    .pipe(
      // tap(console.log)
    )
    .subscribe({
      next: (ev) => { this.handleEvent(ev); },
      error: (e) => { console.log(e); },
      complete: () => { console.log('complete'); }
    });


    this.css();
    this.html();
    this.ts();
    this.links();
    this.description();
  }

  ngAfterViewInit(): void {
    this.initInput(this.button, 'tap');
    this.initInput(this.button, 'click');
    this.counter = this.UIOutput.pipe(
      map( v => this.switchBoard(v) ),
      startWith(this.initState),
      scan((state: State, curr): State => ({ ...state, ...curr })),
      tap( state => this.state = state ),
      switchMap((state: State) =>
      state.count
        ? interval(state.speed).pipe(
            tap(
              _ =>
                (state.countvalue += state.countup ? state.increment : -state.increment)
            )
          )
        : NEVER
    )
    ).subscribe(
      // console.log
    );
  }

  handleEvent(ev: StateEvent): void {
      // console.log('handleEvent');
  }

  initInput(elementrefs: ElementRef[], type: string): void {
    elementrefs.forEach((element) => {
      const source$ = fromEvent<any>(element.nativeElement, type);
      source$.pipe(
         map((event) => {
         const n = event.target.id ? event.target.id : false;
         const v = event.target.value ? parseInt(event.target.value) : false;
         const stateEvent: StateEvent = {name: n, value: v};
         return stateEvent;
         }),
         debounceTime(200),
         distinctUntilChanged(),
       )
       .subscribe(
         (event: StateEvent) => { event.name && this.UIOutput.next(event); }
        );
    });
  }

    switchBoard(event: StateEvent): object {
      switch (event.name) {
        case 'tickSpeed':
          return { speed: event.value };
          break;
        case 'reset':
          return { countvalue: 0 };
          break;
        case 'start':
          return { count: true };
          break;
        case 'pause':
          return { count: false };
           break;
        case 'countup':
          return { countup: true };
            break;
        case 'countdown':
          return {countup: false };
            break;
        case 'setto':
              this.set.countvalue = typeof this.set.countvalue === 'number' ? this.set.countvalue : parseInt(this.set.countvalue);
                return { countvalue: this.set.countvalue };
                break;
        case 'setspeed':
              this.set.speed = typeof this.set.speed === 'number' ? this.set.speed : parseInt(this.set.speed);
                return { speed: this.set.speed };
                break;
        case 'setincrement':
              this.set.increment = typeof this.set.increment === 'number' ? this.set.increment : parseInt(this.set.increment);
              return { increment: this.set.increment };
              break;
        default:
          console.log('unswitched event');
          break;
      }
    }
    ts(): void {
      this.http.get('assets/javascript/stopwatch.component.ts', {responseType: 'text'})
          .subscribe(data => {
            this.code.angularComponent = data;
          });
    }
    html(): void {
      this.http.get('assets/html/stopwatch.component.html', {responseType: 'text'})
          .subscribe(data => {
            this.code.html = data;
          });
    }
    css(): void {
      // this.http.get('assets/css/stopwatch.component.css', {responseType: 'text'})
      //     .subscribe(data => {
      //       this.code.css = data;
      //     });
      return;
    }
    links(): void {
      this.code.links = ['https://www.learnrxjs.io/learn-rxjs/recipes/stop-watch', 'https://www.youtube.com/watch?v=XKfhGntZROQ&feature=youtu.be&ab_channel=ng-conf'];
      return;
    }
    description(): void {
      this.code.description = '\
      CQRS and Event-Sourcing in the frontend \n\
      How to separate rendering and UI interaction\n\
      Stopwatch example bases on "RxJS Advanced Patterns – Operate Heavily Dynamic UI’s" by Michael Hladky\
      ';
    }
}
