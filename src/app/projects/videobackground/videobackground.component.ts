import { HttpClient } from '@angular/common/http';
import { AfterContentInit, AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Codewrapping } from '@src/app/interfaces/codewrap';

@Component({
  selector: 'project-videobackground',
  templateUrl: './videobackground.component.html',
  styleUrls: ['./videobackground.component.css']
})
export class VideobackgroundComponent implements OnInit, AfterContentInit {
  code: Codewrapping = {
    javascript: '',
    html: '',
    css: '',
    description: 'Simple CSS snippet. Dynamic logo on a fullscree video background. Requested by a friend at some point.'
  };
  title = 'video background';
    /* codeWrapper */
    pinned = false;
    togglePinned(): void {
      this.pinned = !this.pinned;
    };
    /* codeWrapper */
  constructor(
    private http: HttpClient
  ) {
    this.css();
    this.html();
    this.js();
  }

  ngOnInit(): void {

  }
  ngAfterContentInit(): void {

  }
  js(): string {
    return '';
  }
  html(): void {
    this.http.get('assets/html/videobackground.component.html', {responseType: 'text'})
        .subscribe(data => {
          this.code.html = data;
        });
  }
  css(): void {
    this.http.get('assets/css/videobackground.component.css', {responseType: 'text'})
        .subscribe(data => {
          this.code.css = data;
        });
  }
}
