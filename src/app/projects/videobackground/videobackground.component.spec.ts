import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideobackgroundComponent } from './videobackground.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('VideobackgroundComponent', () => {
  let component: VideobackgroundComponent;
  let fixture: ComponentFixture<VideobackgroundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideobackgroundComponent ],
      imports: [
        HttpClientTestingModule
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideobackgroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
