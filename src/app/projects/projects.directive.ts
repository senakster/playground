import { Injectable } from '@angular/core';
import { Directive, ViewContainerRef } from '@angular/core';
import { ImagemergerComponent } from './imagemerger/imagemerger.component';
import { VideobackgroundComponent } from './videobackground/videobackground.component';
import { StopwatchComponent } from './stopwatch/stopwatch.component';
import { RestclientComponent } from './restclient/restclient.component';

@Injectable({
  providedIn: 'root'
})
@Directive({
  selector: '[projectHost]'
})

export class ProjectsDirective {


  constructor(
    public viewContainerRef: ViewContainerRef
  ) { }
}

export const ProjectsList = {
    videobackground: VideobackgroundComponent,
    imagemerger: ImagemergerComponent,
    stopwatch: StopwatchComponent,
    restclient: RestclientComponent
};
