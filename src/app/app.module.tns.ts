import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptModule } from '@nativescript/angular';
import { MatIconModule } from '@angular/material/icon';

import { AppRoutingModule } from '@src/app/app-routing.module';
import { AppComponent } from '@src/app/app.component';
import { HomeComponent } from '@src/app/home/home.component';
import { NavigationComponent } from '@src/app/navigation/navigation.component';
import { ContactComponent } from '@src/app/components/contact/contact.component';
import { SnippetsComponent } from '@src/app/components/snippets/snippets.component';

// Uncomment and add to NgModule imports if you need to use two-way binding and/or HTTP wrapper
// import { NativeScriptFormsModule, NativeScriptHttpClientModule } from '@nativescript/angular';
// import { DefaultComponent } from './components/default/default.component';
import { VideobackgroundComponent } from './projects/videobackground/videobackground.component';
import { ImagemergerComponent } from './projects/imagemerger/imagemerger.component';

import { ProjectsDirective } from '@src/app/projects/projects.directive';
import { ErrComponent } from './components/err/err.component';
import { CodewrapComponent } from './components/codewrap/codewrap.component';
import { StopwatchComponent } from './projects/stopwatch/stopwatch.component';
import { AboutComponent } from './components/about/about.component';
import { RestclientComponent } from './projects/restclient/restclient.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavigationComponent,
    ContactComponent,
    SnippetsComponent,
    // DefaultComponent,
    VideobackgroundComponent,
    ImagemergerComponent,
    ProjectsDirective,
    ErrComponent,
    CodewrapComponent,
    StopwatchComponent,
    AboutComponent,
    RestclientComponent,
  ],
  imports: [
    NativeScriptModule,
    AppRoutingModule,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
