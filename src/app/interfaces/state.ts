export interface State {
    count: boolean;
    countup: boolean;
    speed: number;
    countvalue: number;
    increment: number;
  }
export interface StateEvent {
    name: string;
    value: any;
}
export interface SetValues {
  speed: number;
  countvalue: number;
  increment: number;
}

