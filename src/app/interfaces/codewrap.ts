export interface Codewrapping {
    html?: string;
    css?: string;
    scss?: string;
    javascript?: string;
    angularComponent?: string;
    typescript?: string;
    php?: string;
    sql?: string;
    description?: string;
    links?: string[];
}
