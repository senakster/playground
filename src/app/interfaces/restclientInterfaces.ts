export interface User {
    id: number;
    name: string;
    username: string;
    email: string;
    address: object;
    phone: string;
    website: string;
    company: object;
}

export interface Post {
    userId: number;
    id: number;
    title: string;
    body: string;
}
export interface Comment {
    postId: number;
    id: number;
    name: string;
    email: string;
    body: string;
}
export interface Album {
    userId: number;
    id: number;
    title: string;
}
export interface Photo {
    albumId: number;
    id: number;
    title: string;
    url: string;
    thumbnailUrl: string;
}

export interface Todo {
    userId: number;
    id: number;
    title: string;
    completed: boolean;
}

export interface RestData {
    id: number;
}