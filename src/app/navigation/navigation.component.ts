import { Component, OnInit } from '@angular/core';
import { routes } from '@src/app/app.routes';
import { NavigationService } from '@src/app/services/navigation.service';
import { Routes, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  ricons: object = { home: 'home', contact: 'contact', snippets: 'text' };
  r: string[] = [];
  ar: string[] = ['home', 'contact', 'snippets', 'about'];
  navColDefs = '';

  constructor(
    private navService: NavigationService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.r = this.filterRoutes(routes);
  }

  filterRoutes(rts: Routes): string[] {
    let r = [];
    const ncd = [];
    rts.forEach(route => {
      if (route.path.length > 0 && this.ar.includes(route.path)) {
        r = [...r, route.path, ];
        ncd.push('*');
      }
    });
    this.navColDefs = ncd.join(',');
    return r;
  }

  nav(dest: string): void {
    this.navService.nav(dest);
  }

}
