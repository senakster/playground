import { Injectable } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  public url: string;
  constructor(
    public route: ActivatedRoute,
    private router: Router,
  ) {
    this.router.events
    .pipe(
      filter(e => e instanceof NavigationEnd)
    )
    .subscribe((e: NavigationEnd) => {
      this.url = e.url.split('/')[1];
      });
  }

  nav(dest: string): void {
    this.router.navigate([dest]);
  }

}
