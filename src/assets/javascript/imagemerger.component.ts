import { Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { Codewrapping } from '@src/app/interfaces/codewrap';
import mergeImages from 'merge-images';
import { CropperComponent, ImageCropperResult } from 'angular-cropperjs';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'project-imagemerger',
  templateUrl: './imagemerger.component.html',
  styleUrls: ['./imagemerger.component.scss'],
})

export class ImagemergerComponent implements OnInit {
  @ViewChild('angularCropper')
  public angularCropper: CropperComponent;
  @ViewChild('watermark')
  private watermarkElement: ElementRef;
  title = 'Watermarker';


  // cropperjs config
  config = {
    aspectRatio: '16/9',
    checkCrossOrigin: true,
    crossOrigin: true,
    movable: true,
    scalable: true,
    viewMode: 1,
    zoomable: true
  };

  showCropper = true;
  resultImage: any = false; // tmp crop mig
  spinner = 'assets/images/256px-Loadingsome.gif';

  code: Codewrapping = {};


  constructor(
    private sanitizer: DomSanitizer,
    private http: HttpClient
  ) {}


  allowedTypes = [
    'image/jpeg',
    'image/png',
    'image/bmp',
    'image/tiff',
    'image/gif'
  ];

  fileString: any = 'https://scontent-cph2-1.xx.fbcdn.net/v/t31.0-8/11110504_10153335603811667_6504308956680801392_o.jpg?_nc_cat=109&_nc_sid=e3f864&_nc_ohc=i6LStwprMeEAX8QJg5M&_nc_ht=scontent-cph2-1.xx&oh=5ce832927e0ad058e685a6e09250fd53&oe=5F9C605D';
  styles: string[] = ['logo', 'ao', ];
  siURL = 'assets/images/designs/';

  styleImg: object = {ao: `${this.siURL}ao.png`, logo: `${this.siURL}logo.png`};

  result: SafeUrl | boolean = this.spinner;
  resultDisplay = 'hidden';
  dragPosition = { x: 0, y: 0};


  ngOnInit() {

  }


  crop() {
    try {
      this.angularCropper.exportCanvas();
      this.initplacement();
    } catch {
      return;
    }
    this.scrollTo('watermark');
  }

  resultImageFun(event: ImageCropperResult) {
    this.resultImage = this.angularCropper.cropper.getCroppedCanvas().toDataURL('image/jpeg');
  }

  /*
  * Resets watermark placement
  */
  initplacement(): void {
      this.watermarkElement.nativeElement.querySelectorAll('img.overlay').forEach((e) => {
        this.dragPosition = { x: 0, y: 0};
      });
  }

  /*
  * Check if is image and sets resultImage
  * @param event {Event}
  */
  checkstatus(event: any) {
  if (event.blob === undefined) {
    return;
  }
  const urlCreator = window.URL;
  this.resultImage = this.sanitizer.bypassSecurityTrustUrl(
      urlCreator.createObjectURL(new Blob(event.blob)));
}
  /*
  * Sets main image from file or camera
  * @param files {Filelist}
  */
  handleFileInput(files: FileList) {
    const file = files.item(0);
    const fileType = file.type;
    if (file && fileType && this.allowedTypes.includes(fileType)) {
      const myReader: FileReader = new FileReader();
      myReader.readAsDataURL(file);
      myReader.onloadend = (e) => {
        this.fileString = myReader.result;
     };
    } else {
      console.log('File Type not allowed');
    }
  }

  /*
  * Sets main image from url
  * @param files {url}
  */
  handleUrlInput(url: string) {
    this.fileString = url;
  }

  /*
  * prepares the images for merge and calls merge
  * @param src {string}
  */
  async printCard(target): Promise<any> {
    this.resultDisplay = '';
    this.result = this.spinner;
    const pm = await this.imgMeasure(target.parentNode.children[0].src);
    const om = await this.imgMeasure(this.styleImg[target.id]);
    const imgs = await this.joinSize([pm, om]);
    const position = [0, 0];

    // Calculate position
    if (target.parentNode.children[1].style.transform !== '') {
      const ps = target.parentNode.children[0].getBoundingClientRect(); // profile picture display size
      const wp = target.parentNode.children[1].style.transform.split('(')[1].split(','); // watermark position transform style from cdkDrag
      const px = parseInt(wp[0], 10); // transform style x
      const py = parseInt(wp[1], 10); // transform style y
      position[0] = pm.w * (px / ps.width);
      position[1] = pm.h * (py / ps.height);
    }

    const img1 = {
      src: imgs[0].src,
      w: imgs[0].w,
      h: imgs[0].h,
      x: 0,
      y: 0
    };
    const img2 = {
      src: imgs[1].src,
      w: imgs[1].w,
      h: imgs[1].h,
      x: position[0],
      y: position[1],
    };
    this.merge(img1, img2);
    // this.scrollTo('final');
  }

  /*
  * Returns the size of image
  * @param id {string}
  */
  scrollTo(id: string): void {
    const scrollEl = document.getElementById(id);
    window.scroll({
      top: window.pageYOffset + scrollEl.getBoundingClientRect().top,
      behavior: 'smooth'
    });
  }

  /*
  * Returns the size of image
  * @param src {string}
  * @returns {promise}
  */
  imgMeasure(src): Promise<any> {
    return new Promise((resolve, _reject) => {
      const img = new Image();
      img.src = src;
      img.onload = () => {
        const data = {
          src: src,
          w: img.width,
          h: img.height,
          a: img.width / img.height
        };
        resolve(data);
      };
    });
  }

  /*
  * Calculates aspect of final image
  * @param src {array}
  * @returns {Promise}
  */
  joinSize(d): Promise<any> {
    return new Promise((resolve, _reject) => {
      const w1 = d[0].w;
      const h1 = d[0].h;
      let w2 = d[1].w;
      let h2 = d[1].h;

      // Aspect that will fit inside source and is not taller than half of source height
      const aspect = Math.min(w1 / w2, h1 / h2 * .5);
      w2 *= aspect;
      h2 *= aspect;

      const r = [
        {src: d[0].src, w: w1, h: h1},
        {src: this.resize(d[1].src, w2, h2), w: w2, h: h2}
      ];
      resolve(r);
    });
  }

  /*
  * Merges images to the final result and sets result
  * @param src {img, img}
  */
  merge(img1, img2) {
    mergeImages([
      {
      src: img1.src,
      x: img1.x,
      y: img1.y
      },
      {
      src: img2.src,
      x: img2.x,
      y: img2.y
      }
    ])
    .then(
      (merge) => {
      this.result = merge;
      }
    );
  }

  /*
  * Calculates aspect of final image
  * @param src {array}
  * @returns {Promise}
  */
  resize(src, tw, th) {
    const i = new Image();
    i.src = src;
    const canvas = document.createElement('canvas');
    canvas.width = tw;
    canvas.height = th;
    const ctx = canvas.getContext('2d');
    ctx.drawImage(i, 0, 0, tw, th);
    return canvas.toDataURL();
  }
}
