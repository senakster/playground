import { Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { Observable, Subject, fromEvent, interval, NEVER } from 'rxjs';
import { map, scan, startWith, switchMap, tap, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { State, StateEvent } from '@src/app/interfaces/state';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-stopwatch',
  templateUrl: './stopwatch.component.html',
  styleUrls: ['./stopwatch.component.css']
})

export class StopwatchComponent implements OnInit {

  @ViewChildren('input') input: ElementRef[];
  @ViewChildren('button') button: ElementRef[];

  count = 0;
  UIInput: Observable<any>[];
  UIOutput: Subject<any> = new Subject();
  initState: State = {
    count: false,
    speed: 1000,
    countvalue: 0,
    countup: true,
    increment: 1
  };
  state: State;
  set = {speed: this.initState.speed, countvalue: this.initState.countvalue, increment: this.initState.increment };
  counter;



  constructor(
    private http: HttpClient
  ) {
  }

  ngOnInit(): void {
    this.state = this.initState;
    this.UIOutput
    .subscribe({
      next: (ev) => { this.handleEvent(ev); },
      error: (e) => { console.log(e); },
      complete: () => { console.log('complete'); }
    });
  }

  ngAfterViewInit(): void {
    this.initInput(this.button, 'tap'); // init NativeScript mobile user input
    this.initInput(this.button, 'click'); // init web user input
    this.counter = this.UIOutput.pipe(
      map( v => this.switchBoard(v) ),
      startWith(this.initState),
      scan((state: State, curr): State => ({ ...state, ...curr })),
      tap( state => this.state = state ),
      switchMap((state: State) =>
      state.count ?
      interval(state.speed).pipe(
            tap(
              _ =>
                (state.countvalue += state.countup ? state.increment : -state.increment)
            )
          )
      : NEVER
    )
    ).subscribe();
  }

  handleEvent(ev: StateEvent): void {
      console.log('handleEvent');
  }

  // Initiate userinput Web & mobile
  initInput(elementrefs: ElementRef[], type: string): void {
    elementrefs.forEach((element) => {
      const source$ = fromEvent<any>(element.nativeElement, type);
      source$.pipe(
         map((event) => {
         const n = event.target.id ? event.target.id : false;
         const v = event.target.value ? parseInt(event.target.value) : false;
         const stateEvent: StateEvent = {name: n, value: v};
         return stateEvent;
         }),
         debounceTime(200),
         distinctUntilChanged()
       ).subscribe(
         (event: StateEvent) => { event.name && this.UIOutput.next(event); }
        );
    });
  }

    // Handle user input
    switchBoard(event: StateEvent): object {
      switch (event.name) {
        case 'tickSpeed':
          return { speed: event.value };
          break;
        case 'reset':
          return { countvalue: 0 };
          break;
        case 'start':
          return { count: true };
          break;
        case 'pause':
          return { count: false };
           break;
        case 'countup':
          return { countup: true };
            break;
        case 'countdown':
          return {countup: false };
            break;
        case 'setto':
            this.set.countvalue = typeof this.set.countvalue === 'number' ? this.set.countvalue : parseInt(this.set.countvalue);
            return { countvalue: this.set.countvalue };
            break;
        case 'setspeed':
            this.set.speed = typeof this.set.speed === 'number' ? this.set.speed : parseInt(this.set.speed);
            return { speed: this.set.speed };
            break;
        case 'setincrement':
            this.set.increment = typeof this.set.increment === 'number' ? this.set.increment : parseInt(this.set.increment);
            return { increment: this.set.increment };
            break;
        default:
          console.log('unswitched event');
          break;
      }
    }
}
