import { Component, ElementRef, OnDestroy, OnInit, Type, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError, Subject, from, of, NEVER } from 'rxjs';
import { catchError, first, retry, subscribeOn, take, debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { User, Post, Comment, Album, Photo, Todo, RestData } from '@src/app/interfaces/restclientInterfaces'; // data interfaces
import { Codewrapping } from '@src/app/interfaces/codewrap';


@Component({
  selector: 'app-restclient',
  templateUrl: './restclient.component.html',
  styleUrls: ['./restclient.component.css']
})

export class RestclientComponent implements OnInit, OnDestroy {

  @ViewChild('responseData')
  private responseData: ElementRef;

  title = 'REST Client';

  private subs = new SubSink();

  private searchTerms = new Subject<string | false>();

  searchResult$: Observable<object>;

  API_URL = 'https://jsonplaceholder.typicode.com';

  resources = [
    '/users',
    '/posts',
    '/comments',
    '/albums',
    '/photos',
    '/todos',
  ];

  response: object | string;
  currentResource: string;

  searchData = {};
  constructor(
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.searchResult$ = this.searchTerms
    .pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),

      // ignore new term if same as previous term
      distinctUntilChanged(),

      // switch to new search observable each time the term changes
      switchMap( (term) => {
        if (term) {
          const r = this.get(term);
          return r;
        } else {
          return NEVER;
        }
      }),
      tap(console.log)
    );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  getResource(r: string): void {
    console.log(`${r}/1`);
    this.subs.sink = this.get(r)
    .subscribe( (res) => {
      this.searchTerms.next(`${r}/?id=-1`);
      this.response = this.mapData(r, res);
      this.currentResource = r;
    });
  }

  restDelete (target) {
    const data: RestData = this.inputCollect(target);
    if (data.id) {
      const url = `${this.currentResource}/${data.id}`;
      this.delete(url)
      .subscribe( r => {
        console.log('Deleted', r);
      });
    }
  }

  restUpdate(target) {
    const data: RestData = this.inputCollect(target);
    if (data.id) {
      const url = this.currentResource;
      this.put(`${url}/${data.id}`, data)
      .subscribe( r => {
        console.log('Updated', r);
      }
      );
    }

  }

  inputCollect(target): RestData {
    let data: RestData = {id: null};
    const input = target.parentNode.querySelectorAll('input');
    input.forEach((i) => {
      const n = i.name.split('|');
      const v = i.type !== 'checkbox' ? i.value : i.checked;
      const d = {};
      data = this.populateData(n, v, data);
    });
    return data;
  }


  /*
  * Recursive input data collector
  * @Param n {string[]}
  * @Param v {any}
  * @Param data {object}
  * Returns {any | object}
  */
  populateData(n: string[], v: any, data: object): any | object {
    if (n.length >= 1) {
      const k = n.shift();
      if (!data[k]) {
        data[k] = {};
      }
      data[k] = this.populateData(n, v, data[k]);
    } else {
      return v;
    }
    return data;
  }

  getType(v: string): string {
    const re = new RegExp('^(http|https)://', 'i');
    if (re.test(v)) {
      return 'url';
    } else {
      return typeof v;
    }
  }

  search(type: string, value: string | number | boolean) {
    const q = [];
    this.responseData.nativeElement.querySelectorAll('input').forEach((inp) => {
      const name = inp.id;
      const val = inp.type === 'checkbox' ? inp.checked : inp.value;
      if ((val || inp.type === 'checkbox') && inp.id !== 'boolDisable') {
        q.push(`${name}=${val}`);
      }
      if (inp.id === 'boolDisable' && val) {
        q.pop();
      }
    });
    const fetchUrl = `${this.currentResource}/?${q.join('&')}`;
    if (q.length > 0) {
      this.searchTerms.next(fetchUrl);
    }
  }

  /*
  * Recursively maps data types for rendering
  * @Param res {any}
  * Returns {object | string}
  */
  mapData (r: string, res: any): object[] | string {
    if (typeof res === 'object') {
    // assumes uniform data as required by REST manifest
      if (res[0]) {
        const response = [];
        Object.keys(res[0]).forEach((k) => {
          this.searchData[k] = '';
           response.push({
               key: k,
               type: this.mapData(r, res[0][k])
             });
        });
        return response;
      } else {
        return typeof res;
      }
      } else {
          return typeof res;
      }
    }

    // keeps order of objects when iterated in template
    asIsOrder(a, b) {
      return 1;
    }

  // HTTP METHODS
  get(r: string): Observable<any> {
    return this.http.get(`${this.API_URL}${r}`, {responseType: 'json'});
  }

  post(r: string): void {
    const response = this.http.get(`${this.API_URL}${r}`)
    .subscribe(
      console.log
    );
  }

  put(r: string, data: object): Observable<any> {
    return this.http.put(`${this.API_URL}${r}`, {
      body: JSON.stringify(data),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    });
  }

  patch(r: string): void {
    const response = this.http.get(`${this.API_URL}${r}`)
    .subscribe(
      console.log
    );
  }

  delete(r: string): Observable<any> {
    return this.http.delete(`${this.API_URL}${r}`);
  }

}
